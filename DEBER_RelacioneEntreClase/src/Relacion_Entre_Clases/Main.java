package Relacion_Entre_Clases;

public class Main {

	public static void main(String[] args) {
		
		Auto auto1 = new Auto("Chevrolet", "Wind", 1990);
		Auto auto2 = new Auto();
		
		Trailer trailer1 = new Trailer("Hino", "AXB-1", 2005);
		Trailer trailer2 = new Trailer();
		Trailer trailer3 = new Trailer("Mercedes Benz", "QWERTY", 2010);
		
		System.out.println("Marca de Trailer2: "+trailer2.getStrMarca());
		System.out.println("A�o de Trailer2: "+trailer2.getIntA�o());
		
		System.out.println("\nNumero de Llantas de Auto1: "+auto1.llantas.length);
		System.out.println("Numero de Llantas de Trailer1: "+trailer1.llantas.length);
		
		System.out.println("\nCarros en Exitencia: "+Auto.intNumAutos);
		System.out.println("Trailers en Exitencia: "+Trailer.intNumTrailers+"\n\n");
		
		auto1.pitar(1); //PITANDO UNA VEZ
		
		System.out.println("\nCAMBIAR LLANTA DE AUTO 1");
		
		Llanta nuevaLlanta = new Llanta("Chayde-Chayde", 14);
		auto1.cambiarLlanta(nuevaLlanta, 2);

	}

}
