package TipoDeDatoEnumerado;

// if en una sola linea 
//	--> http://lineadecodigo.com/java/el-operador-ternario-en-java/
//	--> http://billyprogramador.blogspot.com/2011/11/if-else-en-una-linea-operador-ternario.html

public enum Color {

	AMARILLO(true), AZUL(true), ROJO(true), NEGRO, BLANCO;
	
	private boolean blnClaro;
	
	private Color() {
		// TODO Auto-generated constructor stub
	}

	private Color(boolean blnTono) {
		this.blnClaro = blnTono;
	}
	
	@Override // .toString() SE PUEDE SOBREESCRIBIR -- .name() NO SE PUEDE SOBREESCRIBIR
	public String toString() {
		// TODO Auto-generated method stub
		
		String strTono = name();
		
		strTono += (blnClaro == true) ? " --> CLARO" : " --> OSCURO";
		
		return strTono;
	}

	public boolean isBlnClaro() {
		return blnClaro;
	}

	public void setBlnClaro(boolean blnClaro) {
		this.blnClaro = blnClaro;
	}
	
	public static void main(String[] args) {
		Color color = Color.AZUL;
		color.setBlnClaro(false);
		
		System.out.println(color.toString());
	}
	
}
