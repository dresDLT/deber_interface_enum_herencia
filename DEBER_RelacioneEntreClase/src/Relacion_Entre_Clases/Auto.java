package Relacion_Entre_Clases;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.Timer;

public class Auto extends Vehiculo implements Pito{
	
	static int intNumAutos=0;
	
	//SOBRE ESCRITURA DE CONSTRUCTOR
	public Auto(String strMarca, String strModelo, int intA�o) {
		super(strMarca, strModelo, intA�o);
		// TODO Auto-generated constructor stub
		llantas = new Llanta[4];
		++intNumAutos;
	}

	public Auto() {
		llantas = new Llanta[4];
		++intNumAutos;
	}
	
	@Override
	void pitar() {
		System.out.println("...PITANDO");	
		for (int i = 0; i < 3; i++) {
			sonarPito();
		}
	}
	
	//SOBRE CARGA DE METODO PITAR
	void pitar (int intVeces){
		System.out.println("PITANDO "+intVeces+" VECES");	
		for (int i = 0; i < intVeces; i++) {
			sonarPito();
		}
	}

	//HAY QUE IMPLEMENTAR TODOS LOS METODOS ABSTRACT DE LA INTERFACE, YA QUE LOS OTROS SON STATICS
	@Override
	public void sonarPito() {
		Toolkit.getDefaultToolkit().beep(); //pa' que suene un sonido
		System.out.println("...PITANDO");
	}

	@Override
	public void metodoA() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void metodoB() {
		// TODO Auto-generated method stub
		
	}

}
