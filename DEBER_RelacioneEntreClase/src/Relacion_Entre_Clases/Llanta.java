package Relacion_Entre_Clases;

public class Llanta {
	private String strMarca;
	private int intRin;
	
	
	//CONSTRUCTOR
	public Llanta(String strMarca, int intRin) {
		this.strMarca = strMarca;
		this.intRin = intRin;
	}
	
	//GETTERS y SETTERS
	public String getStrMarca() {
		return strMarca;
	}
	
	public void setStrMarca(String strMarca) {
		this.strMarca = strMarca;
	}
	
	public int getIntRin() {
		return intRin;
	}
	
	public void setIntRin(int intRin) {
		this.intRin = intRin;
	}
}
