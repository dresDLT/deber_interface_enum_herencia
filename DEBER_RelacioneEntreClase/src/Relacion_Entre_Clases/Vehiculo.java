package Relacion_Entre_Clases;

import java.util.Calendar;
import java.util.Date;

public abstract class Vehiculo {
	String strMarca, strModelo;
	int intA�o;
	Llanta [] llantas;
	
	public Vehiculo(String strMarca, String strModelo, int intA�o) {
		this.strMarca = strMarca;
		this.strModelo = strModelo;
		this.intA�o = intA�o;
	}
	
	public Vehiculo() {
		this.strMarca = "SIN MARCA";
		this.strModelo = "SIN MODELO";
		this.intA�o = new Date().getYear()+1900;
	}
	
	//METODO PITAR
	abstract void pitar(); //PITO, BOCINA, CLAXON
	
	//METODO ACELERAR
	public void acelerar(){
		System.out.println("...ACELERANDO");
	}

	//METODO CAMBIAR LLANTA
	public void cambiarLlanta (Llanta llantaNueva, int intPosicion){
		Llanta llanta = new Llanta(llantaNueva.getStrMarca(), llantaNueva.getIntRin());
		this.llantas[intPosicion-1] = llanta;
		System.out.println("Se cambi� llanta"+intPosicion+" por llanta "+llanta.getStrMarca());
	}
	
	
	//GETTERS y SETTERS
	public String getStrMarca() {
		return strMarca;
	}

	public void setStrMarca(String strMarca) {
		this.strMarca = strMarca;
	}

	public String getStrModelo() {
		return strModelo;
	}

	public void setStrModelo(String strModelo) {
		this.strModelo = strModelo;
	}

	public int getIntA�o() {
		return intA�o;
	}

	public void setIntA�o(int intA�o) {
		this.intA�o = intA�o;
	}

	public Llanta[] getLlanta() {
		return llantas;
	}

	public void setLlanta(Llanta[] llanta) {
		this.llantas = llanta;
	}
	
}
