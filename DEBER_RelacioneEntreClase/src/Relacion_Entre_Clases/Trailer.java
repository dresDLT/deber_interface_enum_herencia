package Relacion_Entre_Clases;

import java.awt.Toolkit;

public class Trailer extends Vehiculo implements Pito{

	static int intNumTrailers=0;
	
	public Trailer(String strMarca, String strModelo, int intA�o) {
		super(strMarca, strModelo, intA�o);
		
		llantas = new Llanta[22];
		intNumTrailers++;
	}
	
	public Trailer() {
		llantas = new Llanta[22];
		intNumTrailers++;
	}

	@Override
	void pitar() {
		System.out.println("...SONANDO CLAXON");
		for (int i = 0; i < 3; i++) {
			sonarPito();
		}
	}

	//HAY QUE IMPLEMENTAR TODOS LOS METODOS ABSTRACT DE LA INTERFACE, YA QUE LOS OTROS SON STATICS
	@Override
	public void sonarPito() {
		Toolkit.getDefaultToolkit().beep(); //pa' que suene un sonido
		System.out.println("...PITANDO");
	}

	@Override
	public void metodoA() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void metodoB() {
		// TODO Auto-generated method stub
		
	}
}
